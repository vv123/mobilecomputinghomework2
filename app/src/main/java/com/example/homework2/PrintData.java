package com.example.homework2;

import java.util.Date;

public class PrintData {

    private String date;
    private String temp;
    private String feels_like;
    private String icon;


    public String getDate() {
        return date;
    }

    public String getFeels_like() {
        return feels_like;
    }

    public String getIcon() {return icon;}
    public String getTemp() {
        return temp;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public void setFeels_like(String feels_like) {
        this.feels_like = feels_like;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
