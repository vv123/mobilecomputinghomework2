package com.example.homework2.Retrofit;
import java.util.ArrayList;
import  java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseData {
   /* @SerializedName("lat")
    public String lat;

    @SerializedName("lon")
    public String lon;*/

    @SerializedName("timezone")
    public String timezone;

    @SerializedName("timezone_offset")
    public String timezone_offset;

    @SerializedName("daily")
    public ArrayList<Daily> daily;
}
