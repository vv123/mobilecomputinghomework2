package com.example.homework2.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeelsLike {

    @SerializedName("day")
    @Expose
    private Double day;


    public Double getDay() {
        return day;
    }

    public void setDay(Double day) {
        this.day = day;
    }


}