package com.example.homework2;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.homework2.Retrofit.Daily;

import java.util.ArrayList;
import java.util.List;


public class DetailFrag extends Fragment {

    private RecyclerView recyclerView;
    private  MainActivity activity;
    private List<PrintData> data;


    public DetailFrag() {
        // Required empty public constructor
    }


    public void setData(List<PrintData> newData) {
        this.data = newData;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         activity = (MainActivity) getActivity();
         data  = activity.getData();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.detail_fragment, container, false);
        recyclerView = rootView.findViewById(R.id.recycler_view);
        MyAdapter mAdapter = new MyAdapter(data);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        
        return rootView;
    }


}