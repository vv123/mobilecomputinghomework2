package com.example.homework2;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import java.util.concurrent.CancellationException;


public class ListFrag extends ListFragment {


    private CityListener cityListener;

    public ListFrag() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

         setListAdapter(new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_list_item_1,
                 getResources().getStringArray(R.array.cities)));
    }

    public interface CityListener
    {
        public void onCitySelected(int index);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try
        {
            cityListener = (CityListener) context;
        }
        catch(ClassCastException e)
        {
            throw new CancellationException(context.toString() + " must implement interface CityListener");
        }
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
       cityListener.onCitySelected(position);
    }
}