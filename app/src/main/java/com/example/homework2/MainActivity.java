package com.example.homework2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.homework2.Retrofit.ApiClient;
import com.example.homework2.Retrofit.ApiInterface;
import com.example.homework2.Retrofit.Daily;
import com.example.homework2.Retrofit.ResponseData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ListFrag.CityListener {

    private static Context context;
    List<PrintData> data = new ArrayList<>();
    DetailFrag df = new DetailFrag();
    private RecyclerView recyclerView;
    MyAdapter mAdapter;

    public List<PrintData> getData(){
        return this.data;
    }

    public static Context getAppContext() {
        return MainActivity.context;
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.context = getApplicationContext();
        setContentView(R.layout.activity_main);
        onCitySelected(0);



        if(findViewById(R.id.layout_default)!= null) {
            FragmentManager manager = this.getSupportFragmentManager();

            manager.beginTransaction().
                    hide(manager.findFragmentById(R.id.detailFrag)).
                    show(manager.findFragmentById(R.id.listFrag)).
                    commit();


        } else if (findViewById(R.id.layout_land)!= null) {
            FragmentManager manager = this.getSupportFragmentManager();

            manager.beginTransaction()
                    .show(manager.findFragmentById(R.id.listFrag))
                    .show(manager.findFragmentById(R.id.detailFrag))
                    .commit();


        }
    }

    @Override
    public void onCitySelected(int index) {

        if(findViewById(R.id.layout_default) != null){
            FragmentManager manager = this.getSupportFragmentManager();
            manager.beginTransaction()
                    .hide(manager.findFragmentById(R.id.listFrag))
                    .show(manager.findFragmentById(R.id.detailFrag))
                    .addToBackStack(null)
                    .commit();

        }
        String[] cities = getResources().getStringArray(R.array.cities);
        String name = cities[index];
        String[] lats =  getResources().getStringArray(R.array.lat);
        String lat = lats[index];
        String[] lons = getResources().getStringArray(R.array.lon);
        String lon = lons[index];
        getWeatherData(lat,lon);
    }

    private void getWeatherData(String lat, String lon) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseData> call = apiInterface.getWeatherData(lat,lon);

        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {

                List<Daily> dailies = response.body().daily;

                data = new ArrayList<>();

                try {
                    for(Daily daily : dailies) {
                        PrintData pd = new PrintData();
                        String date = getDay(daily.getDt());
                        pd.setDate(date);
                        pd.setFeels_like(Integer.valueOf(daily.getFeelsLike().getDay().intValue()).toString() + " °C");
                        pd.setTemp(Integer.valueOf(daily.getTemp().getDay().intValue()).toString() + " °C");
                        pd.setIcon(daily.getWeather().get(0).getIcon());
                        data.add(pd);
                    }


                    recyclerView = findViewById(R.id.recycler_view);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getAppContext()){
                        @Override
                        public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                            // force height of viewHolder here, this will override layout_height from xml
                            lp.width = (int)(getWidth() * 0.95);
                            lp.bottomMargin = 38;
                            lp.topMargin = 32;
                            lp.leftMargin = 20;
                            lp.rightMargin = 20;
                            return true;
                        }
                    });
                    mAdapter = new MyAdapter(data);
                    recyclerView.setAdapter(mAdapter);

                    mAdapter.notifyDataSetChanged();


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {

            }


        });

    }

    private String getDay(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        String day = date.substring(0,2);
        return day;
    }


}