package com.example.homework2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<PrintData> printDataList;
    private ItemClickListener mClickListener;

    public MyAdapter( List<PrintData> itemsData) {
        this.printDataList = itemsData;
        if (itemsData.size() > 0) {

        }
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView dayTextView;
        public TextView feelsLikeTextView;
        public TextView tempTextView;
        public ImageView iconView;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            dayTextView = (TextView) itemView.findViewById(R.id.day);
            feelsLikeTextView = (TextView) itemView.findViewById(R.id.feels_like);
            tempTextView = (TextView) itemView.findViewById(R.id.temp);
            iconView = (ImageView) itemView.findViewById(R.id.icon);

        }
    }


    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View cardView = inflater.inflate(R.layout.cardview, parent, false);


        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(cardView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder holder, int position) {

        PrintData pd = printDataList.get(position);

        TextView dayView = holder.dayTextView;
        dayView.setText(pd.getDate());
        TextView feelsLikeView = holder.feelsLikeTextView;
        feelsLikeView.setText(pd.getFeels_like());
        TextView tempView = holder.tempTextView;
        tempView.setText(pd.getTemp());
        ImageView iconView = holder.iconView;
        String url = "https://openweathermap.org/img/wn/" + pd.getIcon() +"@2x.png";
        Picasso.get()
                .load(url)
                .into(iconView);
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return printDataList.size();
    }
    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}